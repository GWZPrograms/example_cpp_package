//
// Created by wangwei on 18-7-24.
//

#ifndef CONTROLLER_CONTROLLER_H
#define CONTROLLER_CONTROLLER_H

#include <cmath>
#include <string>

namespace GWZPrograms {

class lib1 {
public:
    lib1();
    explicit lib1(float K);
    ~lib1();

    void generateCmd(float sensor, std::string state);
    float getCmd1();
    float getCmd2();

private:
    float cmd1;
    float cmd2;
    float k;
};

}
#endif //MPC_CONTROLLER_CONTROLLER_H
