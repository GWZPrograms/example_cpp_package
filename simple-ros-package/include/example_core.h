#ifndef MPC_CONTROLLER_SRC_MPC_CONTROLLER_CORE_H_
#define MPC_CONTROLLER_SRC_MPC_CONTROLLER_CORE_H_

#include "lib1.h"

#include"ros/ros.h"
#include"std_msgs/Float32.h"
#include"std_msgs/String.h"
#include<string>
#include<vector>
#include<iostream>

namespace GWZPrograms {

class example_core
{
public:
	example_core();
	void callbackState(const std_msgs::String msg);
	void callbackSensorData(const std_msgs::Float32 msg);

	lib1 cmdtest;

	ros::NodeHandle n;
	ros::Subscriber sensor_sub ,state_sub;
    ros::Publisher vel_pub ,str_pub;

	std::string id_;
	bool is_sensor_update;
	bool is_state_update;
	std_msgs::Float32 sensor_data;
	std_msgs::String state;
    std_msgs::Float32 cmd2;
    std_msgs::Float32 cmd1;

	void run();

};
}
#endif /* MPC_CONTROLLER_SRC_MPC_CONTROLLER_CORE_H_ */
